FROM maven:3.3.9-jdk-8-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app

COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]


