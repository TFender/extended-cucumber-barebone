#!/bin/bash

environment=${environment}
cucumberoptions=${cucumberoptions}

sed -i "s/local/$environment/g" src/test/resources/environment.properties
echo "running test for environment: $environment with tag: $cucumberoptions"

mvn test -Dcucumber.options="-t $cucumberoptions"