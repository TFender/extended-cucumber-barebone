package nl.kvk.bestuursverbod.test.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private String projectRoot = "cbr.bestuursverbod.test.firefox.";

    private String readPropertyFromFile(final String property) {
        String firefoxProperty = null;
        InputStream inputStream = PropertyReader.class.getResourceAsStream("/firefox.properties"); //TODO: move this file to resource module
        Properties properties = new Properties();
        try {
            properties.load(inputStream); //TODO: probably dangerous if instantiated multiple times...
            firefoxProperty = properties.getProperty(projectRoot + property);

        } catch(IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch(IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }

        return firefoxProperty;
    }

    public int getProxyType(){
        return Integer.parseInt(readPropertyFromFile("proxy.type"));
    }
    public String getProxyUrl(){
        return readPropertyFromFile("proxy.url");
    }
    public String getProxyPort(){
        return readPropertyFromFile("proxy.port");
    }
    public String getFirefoxPath(){
        return readPropertyFromFile("path");
    }
}

