package Outline;

import Page.StartPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static Enum.Selector.ID;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class StartStepDefinition extends StartPage {

    @Given("^I open google.com$")
    public void iOpenGoogleCom() {
        goToUrl("www.google.com");
    }

    @Then("^I see google.com$")
    public void iSeeGoogleCom() {
       assertThat(waitUntilSelector("hplogo",ID).isDisplayed()).isTrue();
    }
}
