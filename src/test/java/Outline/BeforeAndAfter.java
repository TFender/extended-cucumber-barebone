package Outline;

import Page.BasePage;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import static Enum.Selector.ID;

public class BeforeAndAfter extends BasePage{

    @Before("~@NoBrowser")
    public void beforeScenario() {
        initWebDriver();
        maximizeWindow();
    }

    @After("~@NoBrowser")
    public void afterScenario() {
        endScenario();
    }
}
