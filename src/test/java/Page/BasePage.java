package Page;

import Enum.ElementName;
import Enum.Selector;
import nl.kvk.bestuursverbod.test.common.PropertyReader;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BasePage {

    private static String isLokaal = environmentCheck();
    private static WebDriver driver = null;

    public String getEnvironment(){
        return isLokaal;
    }

    public static String environmentCheck() {
        Properties properties = new Properties();
        InputStream inputStream = BasePage.class.getResourceAsStream("/environment.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("ENVIRONMENT");
    }

    public static void initWebDriver() {
        PropertyReader propertyReader = new PropertyReader();
        try {
            if (isLokaal.equals("local")) {
                File f = new File("./src/main/resources/firefox.properties");
                if(f.exists() && !f.isDirectory() || f.isFile()) {
                    System.out.println("Using firefox with custom properties");
                    FirefoxBinary firefoxBinary = new FirefoxBinary(new File(propertyReader.getFirefoxPath()));
                    FirefoxProfile firefoxProfile = new FirefoxProfile();
                    firefoxProfile.setPreference("network.proxy.type", propertyReader.getProxyType());
                    firefoxProfile.setPreference("network.proxy.http", propertyReader.getProxyUrl());
                    firefoxProfile.setPreference("network.proxy.http_port", propertyReader.getProxyPort());
                    driver = new FirefoxDriver(firefoxBinary, firefoxProfile);
                } else {
                    FirefoxProfile firefoxProfile = new FirefoxProfile();
                    firefoxProfile.setPreference("network.proxy.type", 4);
                    driver = new FirefoxDriver(firefoxProfile);
                }
            } else {
                DesiredCapabilities capability = DesiredCapabilities.firefox();
                driver = new RemoteWebDriver(new URL("http://hub.seleniumgrid.so-test-seleniumgrid.npo00.kvk.nl:4444/wd/hub"), capability);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public String getApplicationUrl(){
        if(isLokaal.equals("DEV")||isLokaal.equals("TST")) return "http://somethingelse.tst.nl";
        if(isLokaal.equals("ACP")) return "http://somethingelse.acp.nl";
        if(isLokaal.equals("PRD")) return "http://somethingelse.prd.nl";
        return "http://google.com";
    }

    public static WebDriver getDriver() {
        if (driver == null) {
            initWebDriver();
        }
        return driver;
    }

    public static void endScenario() {
        driver.quit();
        driver = null;
    }

    // generieke methodes (zoals aanklikken, navigeren)

    public BasePage refresh() {
        getDriver().navigate().refresh();
        return this;
    }

    public BasePage back() {
        getDriver().navigate().back();
        return this;
    }

    public BasePage goToUrl(String Url) {
        if (Url.startsWith("http://"))
            getDriver().navigate().to(Url);
        if (Url.startsWith("www"))
            getDriver().navigate().to("http://" + Url);
        return this;
    }

    public BasePage clickSelector(String selector, Selector selectorType) {
        switch(selectorType){
            case CLASS: getDriver().findElement(By.className(selector)).click(); break;
            case ID: getDriver().findElement(By.id(selector)).click(); break;
            case XPATH: getDriver().findElement(By.xpath(selector)).click(); break;
        }
        return this;
    }

    public WebElement waitUntilSelector(String selector, Selector selectorType) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        WebElement element = null;
        switch (selectorType){
            case XPATH: element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(selector)))); break;
            case ID: element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.id(selector)))); break;
            case CLASS: element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.className(selector)))); break;
        }
        return element;
    }

    public void dangerWait() {
        try {
            Thread.sleep(200);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebElement loopUntilSelector(String selector, Selector selectorType, int loopTimes) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 3);
        WebElement element = null;
        int i=0;
        while (element==null || i <= loopTimes) {
            switch (selectorType) {
                case XPATH:element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(selector))));break;
                case ID:element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.id(selector))));break;
                case CLASS:element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.className(selector))));break;
            }
            if (element!=null) break;
            dangerWait();
            i++;
        }
        return element;
    }

    public BasePage maximizeWindow(){
        getDriver().manage().window().maximize();
        return this;
    }

    public BasePage typeInBySelector(String text, String selector, Selector selectorType) {
        switch (selectorType){
            case CLASS: getDriver().findElement(By.className(selector)).sendKeys(text); break;
            case ID:  getDriver().findElement(By.xpath("//*[@id=\""+selector+"\"]/input")).sendKeys(text); break;
            case XPATH: getDriver().findElement(By.xpath(selector)).sendKeys(text); break;
        }
        return this;
    }

    public BasePage pressKeyBySelector(Keys key, String selector, Selector selectorType) {
        switch (selectorType){
            case ID: getDriver().findElement(By.id(selector)).sendKeys(key); break;
            case XPATH: getDriver().findElement(By.xpath(selector)).sendKeys(key); break;
            case CLASS: getDriver().findElement(By.className(selector)).sendKeys(key); break;
        }
        return this;
    }

    public BasePage mouseHoverBySelector(String selector, Selector selectorType){
        WebElement element = null;
        switch (selectorType){
            case CLASS: element = getDriver().findElement(By.className(selector)); break;
            case XPATH: element = getDriver().findElement(By.xpath(selector)); break;
            case ID: element = getDriver().findElement(By.id(selector)); break;
        }
        Actions action = new Actions(getDriver());
        action.moveToElement(element).build().perform();
        return this;
    }

    public BasePage waitAWhile(long miliseconds){
        getDriver().manage().timeouts().implicitlyWait(miliseconds, TimeUnit.MILLISECONDS);
        return this;
    }

    public String getTextBySelector(String selector, Selector selectorType) {
        String text = null;
        switch (selectorType){
            case ID: text = getDriver().findElement(By.id(selector)).getText(); break;
            case XPATH: text = getDriver().findElement(By.xpath(selector)).getText(); break;
            case CLASS: text = getDriver().findElement(By.className(selector)).getText(); break;
        }
        return text;
    }

    public boolean isPresentBySelector(String selector, Selector selectorType) {
        try {
            switch (selectorType){
                case ID: getDriver().findElement(By.id(selector)); break;
                case XPATH: getDriver().findElement(By.xpath(selector)); break;
                case CLASS: getDriver().findElement(By.className(selector)); break;
            }

        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public boolean checkIfElementContainsText(String text, ElementName elementName){
        List<WebElement> elementList = getDriver().findElements(By.tagName(elementName.toString().toLowerCase()));
        for (WebElement element: elementList) {
            String bodyText = element.getText();
            if (bodyText.contains(text)) return true;
        }
        return false;
    }

    public boolean checkIfTextIsPresent(String text){
        return getDriver().getPageSource().contains(text);
    }

    public WebElement getElementByText(String text){
        return getDriver().findElement(By.xpath("//*[text()='"+text+"']"));
    }


    public BasePage clickDropDownItemBySelector(String selector, Selector selectorType, String dropDownItemName) {
        waitAWhile(1000).clickSelector(selector, selectorType).waitAWhile(1000).getElementByText(dropDownItemName).click();
        return this;
    }

    public void waitTillSelectorIsGone(String selector, Selector selectorType){
        boolean isPresent = true;
        int i = 0;
        while(isPresent || i<30){
            if (isPresentBySelector(selector,selectorType)==false){
                isPresent = false;
            }
            i++;
            dangerWait();
        }
    }
}
