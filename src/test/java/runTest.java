import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/report/cucumber.json",
        overviewReport = true,
        detailedReport = true,
        toPDF = true,
        outputFolder = "target/results-from-report")
@CucumberOptions(tags="@RegressieTest",plugin={ "html:target/report/cucumber-html-report",
        "json:target/report/cucumber.json", "pretty:target/report/cucumber-pretty.txt",
        "usage:target/report/cucumber-usage.json", "junit:target/report/cucumber-results.xml" })
public class runTest {
}

